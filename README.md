This is my attemp at creating a primes multiplication table.

To use first make sure that:
- You are using the specified `ruby` version.
- You have ran `bundle install` to install all the dependencies.
- Make the tool executable by `chmod +x bin/primes_multiplication_table` in order to be able to run the tool.

To run, just do `./bin/primes_multiplication_table`.

If you want to see any other number of primes on the table, please specify it when running the command, `./bin/primes_multiplication_table 3` is for 3 prime numbers.
15 inch screen fits a table with the products of up to 25 primes.
Tables up to 300 primes are generated in a fair amount of time, but unusable since there are no screens big enough.

After 300 the table printing is pretty unusable as it slows down a lot. If you still need the products you can use the
`PrimesMultiplicationTable#primes_products_matrix` which will generate an array of arrays which are the products ordered as in the products matrix but excluding the actuall primes themselves. This works quite nicely for up to 10000 primes (100_000_000 products) primes.