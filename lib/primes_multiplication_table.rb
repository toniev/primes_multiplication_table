require 'terminal-table'

class PrimesMultiplicationTable
  class << self
    def print_primes_products_table(number_of_primes)
      primes = primes(number_of_primes)
      products = primes_products_matrix(primes)
      table = Terminal::Table.new title: "First #{number_of_primes} primes multiplication table"

      table.add_row ['', primes].flatten

      (1..number_of_primes).each_with_index do |num, index|
        table.add_row [primes[index], products[index]].flatten
      end

      puts table
    end

    def primes(amount_of_primes)
      primes = []
      candidate_prime = 1

      while primes.count < amount_of_primes do
        primes << candidate_prime if is_prime?(candidate_prime)
        candidate_prime += 1
      end

      primes
    end

    def primes_products_matrix(primes)
      matrix = []

      primes.each do |prime1|
        row = []
        primes.each do |prime2|
          row << prime1 * prime2
        end
        matrix << row
      end

      matrix
    end

    def is_prime?(number_to_check)
      return false if number_to_check == 0 || number_to_check == 1
      return true if number_to_check == 2 || number_to_check == 3

      square_root = number_to_check ** 0.5

      (2..square_root).each do |number|
        if number_to_check % number == 0 && number < number_to_check
          return false
        end
      end

      return true
    end
  end
end
