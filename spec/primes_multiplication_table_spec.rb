require 'primes_multiplication_table'

describe 'PrimesMultiplicationTable' do
  let(:primes) { PrimesMultiplicationTable.primes 2 }

  it 'generates an array of arrays with the products' do
    products = PrimesMultiplicationTable.primes_products_matrix primes
    expect(products).to be_kind_of Array
    expect(products.first).to be_kind_of Array
  end

  it 'generates a matrix with the products of the first three primes' do
    primes = PrimesMultiplicationTable.primes 3
    products = PrimesMultiplicationTable.primes_products_matrix primes

    expect(products.first).to eq([4, 6, 10])
    expect(products[1]).to    eq([6, 9, 15])
    expect(products.last).to  eq([10, 15, 25])
  end

  it 'generates a specific amount of prime numbers' do
    expect(primes.count).to eq 2
  end

  it 'generates only prime numbers' do
    expect(PrimesMultiplicationTable.is_prime?(primes.first)).to eq true
    expect(PrimesMultiplicationTable.is_prime?(primes.last)).to eq true
  end

  it 'tells if a number is a prime' do
    expect(PrimesMultiplicationTable.is_prime?(131)).to eq true
  end

  it 'tells if a number is not a prime' do
    expect(PrimesMultiplicationTable.is_prime?(4)).to eq false
  end
end
